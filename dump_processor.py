#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import json
import codecs


def is_200(post_body):
    """
    :param post_body:
    :return: true if post contain "200" in HTTP part
    """
    lines = post_body.split("\r\n")
    for line in lines:
        if line.startswith("HTTP"):
            words = line.split(" ")
            if "200" in words:
                return True
    return False


def extract_json_from_post(post_body):
    """
    json starts after empty line from "HOST" (to catch empty jsons).
    :return: json string
    """
    lines = post_body.split("\r\n")
    for line_ind in range(len(lines)):
        if lines[line_ind].lower().startswith("host"):
            if line_ind + 2 < len(lines):
                return lines[line_ind + 2]
            else:
                return ""

def extract_responses_from_post(post_body):
    """
    :param post_body:
    :return: name (starts after empty line after connection) and data. All space are deleting.
    """
    lines = post_body.split("\r\n")
    res = ""
    for line_ind in range(len(lines)):
        if lines[line_ind].lower().startswith("transfer-encoding"):
            name = lines[line_ind + 3]
            break

    for ind in range(line_ind + 4, len(lines)):
        res += lines[ind]
        if lines[ind] == "}":
            break
    return name, res.replace(' ','')

def apply_mask(js_str):
    """
    Applying mask to find similar posts = save only first two leading symbols for every tag and boolean labels.
    :param js_str:
    :return:
    """
    mask = ""

    tag_started = False
    for ind in range(len(js_str) - 2):
        if js_str[ind] == "\"":
            if tag_started:
                tag_started = False
            else:
                tag_started = True
                mask += "_" + js_str[ind + 1] + js_str[ind + 2]
        if js_str[ind] == ":" and (js_str[ind + 1 : ind + 3] == "fa" or js_str[ind + 1 : ind + 3] == "tr"):
            mask += js_str[ind + 1] + js_str[ind + 2]
    return mask


class DumpParser():
    POST_SPLITTER = "------------------------------------------------------------------"
    def __init__(self, path_to_dump):
        self._path_to_dump = path_to_dump
        self.post_bodies = []
        self.jsons_list = []
        self.jsons_dict = {}
        self.responses_list = []
        self.responses_dict = {}

    def read_posts(self):
        """
        This function take a path to file and returns body of all posts as a list.
        :return: list of posts bodies
        """
        try:
            file_obj = codecs.open(self._path_to_dump, "r", "utf_8_sig", errors='ignore' )
            content = file_obj.read()
            #print(content)
            #print("***")
            #print(content2.split(self.POST_SPLITTER))
            #file_obj2.close()


            #file_obj = open(self._path_to_dump, 'rb')
            #content = file_obj.read()#.decode("utf-8")

            #content = str(content).replace('b\'', '')
            #print(content)


            self.post_bodies = content.split(self.POST_SPLITTER)
            return self.post_bodies
        except IOError:
            print("Problem with path to file!")
        finally:
            file_obj.close()

    def filter_200(self):
        """
        :return: list of 200-posts bodies with
        """
        self.post_bodies = list(filter(is_200, self.post_bodies))
        return self

    def extract_jsons(self):
        """
        Create list and enumerate dictionary of jsons.
        json starts after empty line from "HOST" (to catch empty jsons).
        :return: list of json
        """
        self.jsons_list = [extract_json_from_post(post) for post in self.post_bodies]
        self.jsons_dict = {num: j for num, j in enumerate(self.jsons_list)}
        return self

    def extract_data(self):
        """
        Create list and enumerate dictionary of jsons.
        json starts after empty line from "HOST" (to catch empty jsons).
        :return: list of json
        """
        self.responses_list = [extract_responses_from_post(post) for post in self.post_bodies]
        self.responses_dict = {num: j for num, j in enumerate(self.responses_list)}
        return self

    def convert_string_to_jsons(self):
        """
        Converts string to jsons
        :return:
        """
        self.jsons_list = list(map(json.loads, self.jsons_list))
        self.jsons_dict = {num: j for num, j in enumerate(self.jsons_list)}
        self.responses_list = list(map(json.loads, self.responses_list))
        self.responses_dict = {num: j for num, j in enumerate(self.responses_list)}
        return self

    def remove_similar_jsons(self):
        """
        Remove jsons with identical set of tags
        :return:
        """
        jsons_masked = []
        jsons_unique = []
        for js in self.jsons_list:
            mask = apply_mask(js)

            if (mask not in jsons_masked):
                jsons_masked.append(mask)
                jsons_unique.append(js)
        self.jsons_list = jsons_unique
        self.jsons_dict = {num: j for num, j in enumerate(self.jsons_list)}
        return self

    def remove_identical_and_empty(self):
        """
        Remove __
        :return:
        """

        jsons_unique = []
        response = []
        ind = 0
        for js in self.jsons_list:
            if (js not in jsons_unique and js != ""):
                jsons_unique.append(js)
                if (ind < len(self.responses_list)):
                    response.append(self.responses_list[ind])
            ind += 1
        self.jsons_list = jsons_unique
        self.jsons_dict = {num: j for num, j in enumerate(self.jsons_list)}
        self.response_list = response
        self.response_dict = {num: j for num, j in enumerate(self.response_list)}
        return self

    def remove_similar_data(self):
        """
        Remove data with identical set of tags
        :return:
        """
        responses_masked = []
        responses_unique = []

        for d in self.jsons_list:
            mask = apply_mask(d)
            if (mask not in responses_masked):
                responses_masked.append(mask)
                responses_unique.append(d)
        self.responses_list = responses_unique
        self.responses_dict = {num: j for num, j in enumerate(self.responses_list)}
        return self



test_parser = DumpParser("small_dump.txt")
test_parser.read_posts()
print("All posts: ", len(test_parser.post_bodies))
jsons = test_parser.filter_200().extract_jsons().remove_identical_and_empty().convert_string_to_jsons().jsons_list
print("Success posts:", len(jsons))
#print("Unique posts:", len(test_parser.extract_jsons().remove_similar_jsons().jsons_list))


data = jsons[0]

#data = json.loads(example)
print(data['groups'][0]['mgid'])

#print(test_parser.extract_json())
#print(test_parser.extract_data())

#print(test_parser.convert_string_to_jsons())

